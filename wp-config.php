<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'pavlinawp');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'bGl`}_*<t{t~445;q@8/(^]?lZvx-p,> #?!Z.}6FYUNT,q*5qB}9dRd.sRJ@[%f');
define('SECURE_AUTH_KEY',  'mePf9Q,*~E<1zRT]gtfIR)UUUxu7G6Ds59NVj/Aac:/i|bG*A$ldi:N^f;a43o6t');
define('LOGGED_IN_KEY',    'u.w?M9NKo/yMY.[4Tzumzk&+f`Z2eBG<kY2:$PDfzgF.$zmK%wK/9l)6EGMZ]O6r');
define('NONCE_KEY',        '?TxB;}+Mf=_=mL+8n%nP([;N~SfI]8r,<-3Q:xj(`XBqXyR:xe7]k=`L o>+oz)6');
define('AUTH_SALT',        'b$uHtk)6|:L`z2@^l-`3U~1Q34XE}ItPnN&-/NzEI*L=vbP-GshI6G_2v&`Yjzi$');
define('SECURE_AUTH_SALT', 'E_aa4I5SGmS+_}p2CXw&ItA#Loi}=t5FR1cke]aqBmOd]Y~>Y2hFT/^+Q{GDu}7[');
define('LOGGED_IN_SALT',   '+rY6mxsho*UZsG8`jKS@[g_/KMma4tLKSW5fE=TYt(eG%7%jX`wMx&L1Y$YA_5{2');
define('NONCE_SALT',       'wRe /8jKoCMqdaq?de%j||*HS(u8hW76|[MB<cZdvS@zXO2+M);$bckW=bB|NF53');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
