��          D      l       �   �   �   	   �     �     �  (  �    �  	   �     �     	                          Metro pirate is a metro WordPress theme for blogging, with a minimalist, clean design. The theme is dominated by a white background with a colorful grid style. Metro pirate has a fully responsive layout, and comes with many useful customization options. Themeisle https://themeisle.com metro-pirate PO-Revision-Date: 2016-07-21 13:05:50+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: GlotPress/2.4.0-alpha
Language: cs_CZ
Project-Id-Version: Themes - metro-pirate
 Metro Pirate je metro WordPress šablona pro blogování, s minimalistickým, uceleným vzhledem. Šabloně dominuje bílé pozadí a barevný mřížkový styl. Metro Pirate má plně responzivní rozvržení a nabízí možnosti mnoha užitečných přizpůsobení. ThemeIsle https://themeisle.com Metro-Pirate 